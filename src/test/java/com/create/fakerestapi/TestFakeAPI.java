package com.create.fakerestapi;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;

public class TestFakeAPI {
	
	@Test
	public void test_get_user() {
		
		baseURI = "http://localhost:3000/";
		given().
			param("firstname", "Tshwarelo"). // fetch only user with the name Tshwarelo
			get("/users").
	    then().
	    	statusCode(200).
	    	log().all();
	}

	@Test
	public void test_get_subject() {
		
		baseURI = "http://localhost:3000/";
		given().
			param("id", 1). // fetch only subject with the id 1
			get("/subjects").
	    then().
	    	statusCode(200).
	    	log().all();
	}
	
	@Test
	public void test_post_user() {
		
		baseURI	= "http://localhost:3000/";
		
		JSONObject request = new JSONObject();
		request.put("firstname", "Mokgadi");
		request.put("lastname", "Mabotja");
		request.put("subjectid", 1);
		
		given().
			header("Content-Type", "application/json").
			contentType(ContentType.JSON).accept(ContentType.JSON).
			body(request.toJSONString()).
		when().
			post("/users").
		then().
			statusCode(201).log().all();
	}
	
	@Test
	public void test_patch_user() {
		
		baseURI	= "http://localhost:3000/";
		
		JSONObject request = new JSONObject();
		request.put("firstname", "Morotola");
		
		given().
			header("Content-Type", "application/json").
			contentType(ContentType.JSON).accept(ContentType.JSON).
			body(request.toJSONString()).
		when().
			patch("/users/4").
		then().
			statusCode(200).log().all();
	}
	
	@Test
	public void test_put_user() {
		
		baseURI	= "http://localhost:3000/";
		
		JSONObject request = new JSONObject();
		request.put("firstname", "Cynthia");
		request.put("lastname", "Mabotja");
		request.put("subjectid", 1);
		
		given().
			header("Content-Type", "application/json").
			contentType(ContentType.JSON).accept(ContentType.JSON).
			body(request.toJSONString()).
		when().
			put("/users/4").
		then().
			statusCode(200).log().all();
	}
	
	@Test
	public void test_delete_user() {
		
		baseURI	= "http://localhost:3000/";
		
		when().
			delete("/users/4").
		then().
			statusCode(200).log().all();
	}
}
