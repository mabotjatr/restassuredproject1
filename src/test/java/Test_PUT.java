import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;



public class Test_PUT {
	
	@Test
	public void test_1() {
		
		JSONObject request = new JSONObject();
		request.put("name", "Tshwarelo");
		request.put("job", "Developer");
		
		given().
			headers("Content-Type", "application/json").
			contentType(ContentType.JSON).accept(ContentType.JSON).
			body(request.toJSONString()).
		when().
			put("https://reqres.in/api/users/2"). // the PUT endpoint
		then().
			statusCode(200).
			log().all();
	}

}
