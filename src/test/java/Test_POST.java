import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.internal.path.json.mapping.JsonObjectDeserializer;
import static io.restassured.RestAssured.*;
import java.util.HashMap;
import java.util.Map;

public class Test_POST {

	@Test
	public void test_1() {
		
		Map<String, Object> map  =  new HashMap<String, Object>();
		map.put("name", "Tshwarelo");
		map.put("job", "Developer");
		
		JSONObject request = new JSONObject(map);
		
		given().
			headers("Content-Type", "application/json").
			contentType(ContentType.JSON).accept(ContentType.JSON).
			body(request.toJSONString()).
		when().
			post("https://reqres.in/api/users"). // the POST endpoint
		then().
			statusCode(201).
			log().all();
	}
}
