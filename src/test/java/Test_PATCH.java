import static io.restassured.RestAssured.given;

import java.sql.Timestamp;
import java.util.Date;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;

public class Test_PATCH {
	
	@Test
	public void test_patch() {
		
		JSONObject request = new JSONObject();
		request.put("name", "Tshwarelo");
		request.put("job", "Developer");
		request.put("location", "Midrand");

		
		given().
			headers("Content-Type", "application/json").
			contentType(ContentType.JSON).accept(ContentType.JSON).
			body(request.toJSONString()).
		when().
			patch("https://reqres.in/api/users/2"). // the PATCH end point
		then().
			statusCode(200).
			log().all();
	}

}
